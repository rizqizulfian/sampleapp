<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class PerkalianController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('test');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $nilai1 =$request->input('nilai1');
        $nilai2 =$request->input('nilai2');
        $operator=$request->input('operator');
        if ($operator == "tambah")
        {
            $hasil = $nilai1+$nilai2;
            dd($hasil);
        }
        else if($operator == "kurang")
        {
            $hasil= $nilai1-$nilai2;
            dd($hasil);
        }
        else if($operator == "kali")
        {
            $hasil= $nilai1*$nilai2;
            dd($hasil);
        }
        else if($operator == "bagi")
        {
            $hasil= $nilai1/$nilai2;
            dd($hasil);
        }
        else
        {
            $hasil= pow($nilai1,$nilai2);
            dd($hasil);
            // return \Redirect::route('perkalian.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
